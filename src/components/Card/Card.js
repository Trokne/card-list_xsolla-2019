import React from 'react';
import './Card.css';

class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      imageSource: null,
      isSelected: false
    };
  }

  changeSelecting = () => this.setState({ isSelected: !this.state.isSelected });

  render() {
    return (
      <div
        className={this.state.isSelected ? 'box selectedCard' : 'box'}
        onClick={this.changeSelecting}
      >
        <img
          className='image'
          src={this.props.imageSource}
          alt='Изображение менеджмента'
        />
        <div className='text'>
          <p className='header'>{this.props.header}</p>
          <p className='description'>{this.props.description}</p>
        </div>
      </div>
    );
  }
}
export default Card;
