import React from 'react';
import './Cards.css';
import Card from '../Card/Card';
import cardsData from '../../assets/cards.json';

class Cards extends React.Component {
  constructor(props) {
    super(props);
    this.state = { cards: this.getCards() };
  }
  getCards() {
    return cardsData.cards.map((card, index) => {
      return (
        <div className='card' key={index}>
          <Card
            header={card.category}
            description={card.text}
            imageSource={card.image}
          />
        </div>
      );
    });
  }

  render() {
    return <div className='cards'>{this.state.cards}</div>;
  }
}
export default Cards;
