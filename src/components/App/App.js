import React from 'react';
import './App.css';
import Cards from '../Cards/Cards';

class App extends React.Component {
  render() {
    return (
      <div className='app'>
        <Cards />
      </div>
    );
  }
}

export default App;
